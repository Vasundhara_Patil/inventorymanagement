using System.Net;
using InventoryProj.Models;
using InventoryProj.Models.Repository.ProductRepository;
using Microsoft.AspNetCore.Mvc;

namespace InventoryProj.Controllers
{
    [Route("api/[controller]")]

    public class ProductController : Controller
    {
        IProductRepository _repository;
        public ProductController(IProductRepository repository) => _repository = repository;

        /// <summary>
        /// To Get Paging List of Products
        /// </summary>
        [Route("list")]
        [HttpGet]
        public IActionResult GetProductDetails(int PageNumber, int PageSize)
        {
            try
            {
                PagingParams productReq = new PagingParams
                {
                    PageNumber = PageNumber,
                    PageSize = PageSize
                };

                var Results = _repository.GetProductDetails(productReq);
                PagedList pg = new PagedList(Results, productReq.PageNumber, productReq.PageSize);
                var outputModel = new ProductOutputModel
                {
                    Paging = pg.GetHeader(),
                    Links = CommonController.Instance.GetLinks(pg, productReq, Request.Scheme + "://" + Request.Host + Request.PathBase + Request.Path),
                    Items = pg.List
                };
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", outputModel);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
        /// <summary>
        /// To Get Paging List of Products
        /// </summary>
        [Route("list")]
        [HttpPost]
        public IActionResult GetProductDetails(PagingParams reqModel)
        {
            try
            {
                var Results = _repository.GetProductDetails(reqModel);
                PagedList pg = new PagedList(Results, reqModel.PageNumber, reqModel.PageSize);
                var outputModel = new ProductOutputModel
                {
                    Paging = pg.GetHeader(),
                    Links = CommonController.Instance.GetLinks(pg, reqModel, Request.Scheme + "://" + Request.Host + Request.PathBase + Request.Path),
                    Items = pg.List
                };
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", outputModel);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
        /// <summary>
        /// To Insert Product
        /// </summary>
        [Route("InsertUpdate")]
        [HttpPost]
        public IActionResult InsertUpdateProduct([FromBody]ProductModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var Results = _repository.InsertUpdateProduct(model);
                    return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", Results);
                }
                else
                    return BadRequest(ModelState);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
    }
}