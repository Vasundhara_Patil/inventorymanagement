using System;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Collections.Generic;
using InventoryProj.Models;

namespace InventoryProj.Controllers
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class CommonController : Controller
    {

        private static CommonController instance = null;
        public static CommonController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CommonController();
                }
                return instance;
            }
        }

        private CommonController()
        {

        }

        /// <summary>
        /// Get the HTTP Response
        /// </summary>
        /// <param name="statuscode">HTTP Status Code</param>
        /// <param name="message">To return in response as message</param>
        /// <param name="Data">Data to return in HTTP Response</param>
        /// <returns></returns>
        public IActionResult ReturnResponse(int statuscode, string message, String Data)
        {
            try
            {
                return StatusCode(statuscode,new RequestResult
                {
                    Results = new { Status = statuscode, Msg = message },
                    Msg = message,
                    Data = Data
                });
            }
            catch (Exception ex)
            {
                return StatusCode(ResponseStatusCode.Failed,new RequestResult
                {
                    Results = new { Status = RequestState.Failed, Msg = "CC_01" },
                    Msg = "Failed",
                    Data = ex.Message
                });
            }

        }

        public IActionResult ReturnResponse(int statuscode, string message, Object Data)
        {
            try
            {
                return StatusCode(statuscode, new RequestResult
                {
                    Results = new { Status = statuscode, Msg = message },
                    Msg = message,
                    Data = Data
                });
            }
            catch (Exception ex)
            {
                return StatusCode(ResponseStatusCode.Failed, new RequestResult
                {
                    Results = new { Status = RequestState.Failed, Msg = "CC_01" },
                    Msg = "Failed",
                    Data = ex.Message
                });
            }

        }

        /// <summary>
        /// Get the HTTP Response
        /// </summary>
        /// <param name="statuscode">HTTP Status Code</param>
        /// <param name="message">To return in response as message</param>
        /// <param name="Data">Data to return in HTTP Response</param>
        /// <returns></returns>
        public IActionResult ReturnResponse(int statuscode, string message, DataSet Data)
        {
            try
            {
                return StatusCode(statuscode, new RequestResult
                {
                    Results = new { Status = statuscode, Msg = message },
                    Msg = message,
                    Data = Data
                });
            }
            catch (Exception ex)
            {
                return StatusCode(ResponseStatusCode.Failed, new RequestResult
                {
                    Results = new { Status = RequestState.Failed, Msg = "CC_01" },
                    Msg = "Failed",
                    Data = ex.Message
                });
            }

        }


        public List<LinkInfo> GetLinks(PagedList list, PagingParams productReq, string routeName)
        {
            var links = new List<LinkInfo>();

            if (list.HasPreviousPage)
                links.Add(CreateLink(routeName, list.PreviousPageNumber,
                           list.PageSize, "previousPage", "GET"));


            if (list.HasNextPage)
            {
                links.Add(CreateLink(routeName, list.NextPageNumber,
                           list.PageSize, "nextPage", "GET"));
            }
            else
            {
                links.Add(CreateLink(routeName, list.PreviousPageNumber,
                list.PageSize, "previousPage", "GET"));
            }

            return links;
        }

        public LinkInfo CreateLink(string routeName, int pageNumber, int pageSize,string rel, string method)
        {
            return new LinkInfo
            {
                Href = routeName + "?PageNumber=" + pageNumber + "&PageSize=" + pageSize ,
                Rel = rel,
                Method = method
            };
        }

    }

}