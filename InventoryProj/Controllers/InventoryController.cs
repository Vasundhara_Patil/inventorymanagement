using System.Net;
using InventoryProj.Models;
using InventoryProj.Models.Repository.InventoryRepository;
using Microsoft.AspNetCore.Mvc;

namespace InventoryProj.Controllers
{
    [Route("api/[controller]")]

    public class InventoryController : Controller
    {
        IInventoryRepository _repository;
        public InventoryController(IInventoryRepository repository) => _repository = repository;
        /// <summary>
        /// To Get Supplier List for Invetory Addition
        /// </summary>
        [Route("Supplier/List")]
        [HttpGet]
        public IActionResult GetSupplierList()
        {
            try
            {
                var Results = _repository.GetSupplierList();
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", Results);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }

        /// <summary>
        /// To Get list of Product added in inventory
        /// </summary>
        [Route("list")]
        [HttpGet]
        public IActionResult GetInventoryDetails(int PageNumber, int PageSize)
        {
            try
            {
                PagingParams PageReq = new PagingParams
                {
                    PageNumber = PageNumber,
                    PageSize = PageSize
                };

                var Results = _repository.GetInventoryDetails(PageReq);
                PagedList pg = new PagedList(Results, PageReq.PageNumber, PageReq.PageSize);
                var outputModel = new ProductOutputModel
                {
                    Paging = pg.GetHeader(),
                    Links = CommonController.Instance.GetLinks(pg, PageReq, Request.Scheme + "://" + Request.Host + Request.PathBase + Request.Path),
                    Items = pg.List
                };
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", outputModel);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
        /// <summary>
        /// To Get list of Product added in inventory
        /// </summary>
        [Route("list")]
        [HttpPost]
        public IActionResult getProductList(PagingParams reqModel)
        {
            try
            {
                var Results = _repository.GetInventoryDetails(reqModel);
                PagedList pg = new PagedList(Results, reqModel.PageNumber, reqModel.PageSize);
                var outputModel = new ProductOutputModel
                {
                    Paging = pg.GetHeader(),
                    Links = CommonController.Instance.GetLinks(pg, reqModel, Request.Scheme + "://" + Request.Host + Request.PathBase + Request.Path),
                    Items = pg.List
                };
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", outputModel);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
        /// <summary>
        /// To Insert Stock of Product in Inventory
        /// </summary>
        [Route("InsertUpdate")]
        [HttpPost]
        public IActionResult insertUpdateInventory([FromBody] InventoryModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var Results = _repository.InsertUpdateInventory(model);
                    return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", Results);
                }
                else
                    return BadRequest(ModelState);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
    }
}