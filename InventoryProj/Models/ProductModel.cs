
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;

namespace InventoryProj.Models
{
    
    public class ProductModel
    {
        [JsonIgnore]
        public IConfiguration _iConfiguration;
        public ProductModel(IConfiguration IConfiguration) => _iConfiguration = IConfiguration;
        /// <summary>
        /// Enter Name of Product
        /// </summary>
        [Required(ErrorMessage ="Product Name is Required")]
        public string productName { get; set; }
        /// <summary>
        /// Enter Id of Product, at insertion is may be 0 or null but for Update & delete Specify Product Id
        /// </summary>
        public int? productId { get; set; }
        /// <summary>
        /// Enter Description of Product
        /// </summary>
        public string productDescription { get; set; }
        /// <summary>
        /// Operation Flag: Insert =1,Update=2,Delete:3
        /// </summary>
        [Required(ErrorMessage = "Operation Flag is Required")]
        [Range(1, 3, ErrorMessage = "Enter Operation Flag in between 1 to 3")]
        public int operationFlag { get; set; }
        /// <summary>
        /// User Id Who is performing operations
        /// </summary>
        public string createdModifyBy { get; set; }
        

        public DataSet GetProductDetails(PagingParams reqModel)
        {
            string Connection = _iConfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("GetProductDetails");
            DataSet ds = new DataSet();
            using (command)
            {
                try
                {
                    database.AddInParameter(command, "@PageNumber", DbType.Int32, reqModel.PageNumber);
                    database.AddInParameter(command, "@PageSize", DbType.Int32, reqModel.PageSize);
                    ds = database.ExecuteDataSet(command);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return ds;
            }
        }

        public object InsertUpdateProduct()
        {
            string Connection = _iConfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("ProductInUp");
            JObject Result = new JObject();
            using (command)
            {
                try
                {
                    database.AddParameter(command, "@ProductId", System.Data.DbType.Int32, System.Data.ParameterDirection.InputOutput, "ProductId", System.Data.DataRowVersion.Default, productId);

                    database.AddInParameter(command, "@ProductName", DbType.String, productName);
                    database.AddInParameter(command, "@ProductDescription", DbType.String, productDescription);
                    database.AddInParameter(command, "@CrModBy", DbType.String, createdModifyBy);
                    database.AddInParameter(command, "@OperationFlag", DbType.Int32, operationFlag);
                    database.AddOutParameter(command, "@Result", DbType.Int32, -1);
                    database.ExecuteNonQuery(command);

                    Result.Add("productId", command.Parameters["@ProductId"].Value.ToString());
                    Result.Add("result", command.Parameters["@Result"].Value.ToString());
                    return Result;
                }
                catch (Exception e)
                {
                    Result.Add("result", "-10");
                    Result.Add("message", e.Message);
                    return Result;
                }
            }
        }
    }
}
