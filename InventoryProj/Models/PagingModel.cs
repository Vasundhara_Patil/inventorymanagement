using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
namespace InventoryProj.Models
{
    public class PagingModel 
    {
     
    }
    public class ProductReqModel
    {

        public int PageNumber { get; set; } = 1;

        public int PageSize { get; set; } = 20;
    }
    public class PagingParams
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 5;
    }
    public class ProductOutputModel
    {
        public PagingHeader Paging { get; set; }
        public List<LinkInfo> Links { get; set; }
        public Object Items { get; set; }
    }
    public class LinkInfo
    {
        public string Href { get; set; }
        public string Rel { get; set; }
        public string Method { get; set; }
    }
    public class PagingHeader
    {
        public PagingHeader(
           int totalItems, int pageNumber, int pageSize, int totalPages)
        {
            this.TotalItems = totalItems;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.TotalPages = totalPages;
        }

        public int TotalItems { get; }
        public int PageNumber { get; }
        public int PageSize { get; }
        public int TotalPages { get; }
        //public string ToJson() => JsonConvert.SerializeObject(this,
        //                            new JsonSerializerSettings
        //                            {
        //                                ContractResolver = new
        //            CamelCasePropertyNamesContractResolver()
        //                            });

    }

    public class PagedList
    {
        public PagedList(DataSet ds, int pageNumber, int pageSize)
        {
            this.TotalItems = Convert.ToInt32(ds.Tables["ItemCount"].Rows[0]["TotalRows"]);
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.List = ds.Tables[1];
        }

        public int TotalItems { get; }
        public int PageNumber { get; }
        public int PageSize { get; }
        public DataTable List { get; }
        public int TotalPages =>
              (int)Math.Ceiling(this.TotalItems / (double)this.PageSize);
        public bool HasPreviousPage => this.PageNumber > 1;
        public bool HasNextPage => this.PageNumber < this.TotalPages;
        public int NextPageNumber =>
               this.HasNextPage ? this.PageNumber + 1 : this.TotalPages;
        public int PreviousPageNumber =>
               this.HasPreviousPage ? this.PageNumber - 1 : 1;

        public PagingHeader GetHeader()
        {
            return new PagingHeader(
                 this.TotalItems, this.PageNumber,
                 this.PageSize, this.TotalPages);
        }
    }
}