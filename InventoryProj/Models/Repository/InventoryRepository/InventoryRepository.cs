using System.Data;
using System.Collections.Generic;
using InventoryProj.Models;
using Microsoft.Extensions.Configuration;

namespace InventoryProj.Models.Repository.InventoryRepository
{
    public class InventoryRepository : IInventoryRepository
    {
        InventoryModel _model;
        IConfiguration _iConfiguration;

        public InventoryRepository(IConfiguration IConfiguration)
        {
            _iConfiguration = IConfiguration;
            _model = new InventoryModel(_iConfiguration);
        }
        public DataSet GetSupplierList()
        {
            return _model.GetSupplierList().GetTableName();
        }

        public DataSet GetInventoryDetails(PagingParams reqModel)
        {
            return _model.GetInventoryDetails(reqModel).GetTableName();
        }

        public object InsertUpdateInventory(InventoryModel model)
        {
            _model = model;
            _model._iConfiguration = _iConfiguration;
            return _model.InsertUpdateInventory();
        }
    }
}