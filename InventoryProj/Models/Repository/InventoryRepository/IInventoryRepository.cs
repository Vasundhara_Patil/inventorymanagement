using System;
using System.Data;
using InventoryProj.Models;

namespace InventoryProj.Models.Repository.InventoryRepository
{
    public interface IInventoryRepository
    {
        DataSet GetSupplierList();
        DataSet GetInventoryDetails(PagingParams reqModel);
        object InsertUpdateInventory(InventoryModel model);
    }
}