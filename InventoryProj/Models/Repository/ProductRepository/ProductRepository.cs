using System.Data;
using System.Collections.Generic;
using InventoryProj.Models;
using Microsoft.Extensions.Configuration;

namespace InventoryProj.Models.Repository.ProductRepository
{
    public class ProductRepository : IProductRepository
    {
        ProductModel _model;
        IConfiguration _iConfiguration;

        public ProductRepository(IConfiguration IConfiguration)
        {
            _iConfiguration = IConfiguration;
            _model = new ProductModel(_iConfiguration);
        }

        public DataSet GetProductDetails(PagingParams reqModel)
        {
            return _model.GetProductDetails(reqModel).GetTableName();
        }

        public object InsertUpdateProduct(ProductModel model)
        {
            _model = model;
            _model._iConfiguration = _iConfiguration;
            return _model.InsertUpdateProduct();
        }
    }
}