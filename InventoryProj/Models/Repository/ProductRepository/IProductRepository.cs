using System;
using System.Data;
using InventoryProj.Models;

namespace InventoryProj.Models.Repository.ProductRepository
{
    public interface IProductRepository
    {
        DataSet GetProductDetails(PagingParams reqModel);
        object InsertUpdateProduct(ProductModel model);
    }
}