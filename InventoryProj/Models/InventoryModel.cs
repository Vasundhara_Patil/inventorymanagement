
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;

namespace InventoryProj.Models
{
    public class InventoryModel
    {
        [JsonIgnore]
        public IConfiguration _iConfiguration;
        public InventoryModel(IConfiguration IConfiguration) => _iConfiguration = IConfiguration;
        /// <summary>
        /// Enter Id of Inventory, at insertion is may be 0 but for Update and delete Specify inventory Id
        /// </summary>
        public int inventoryId { get; set; }
        /// <summary>
        /// Enter Id of Supplier
        /// </summary>
        
        [Required(ErrorMessage = "Supplier Id is Required"),Range(1,Int64.MaxValue,ErrorMessage ="Supplier Id Should be minimum 1")]
        public int? supplierId { get; set; }
        /// <summary>
        /// Enter Id of Product
        /// </summary>
        [Required(ErrorMessage = "Product Id is Required"), Range(1, Int64.MaxValue, ErrorMessage = "Product Id Should be minimum 1")]
        public int? productId { get; set; }
        /// <summary>
        /// Enter Quantity of Product
        /// </summary>
        [Required(ErrorMessage = "Product Quantity is Required"),Range(1, 100, ErrorMessage = "Enter Quantity Flag in between 1 to 100")]
        public int? quantity { get; set; }
        /// <summary>
        /// Enter Price of Product
        /// </summary>
        [Required(ErrorMessage = "Product Price is Required"), Range(1, Int64.MaxValue, ErrorMessage = "Product Price Should be minimum 1")]
        public decimal? price { get; set; }
        /// <summary>
        /// Operation Flag: Insert =1,Update=2,Delete:3
        /// </summary>
        [Required(ErrorMessage = "Operation Flag is Required"), Range(1, 3, ErrorMessage = "Enter Operation Flag in between 1 to 3")]
        public int? operationFlag { get; set; }
        /// <summary>
        /// User Id Who is performing operations
        /// </summary>
        public string createdModifyBy { get; set; }

        public DataSet GetSupplierList()
        {
            string Connection = _iConfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("GetSupplierDetails");
            DataSet ds = new DataSet();
            using (command)
            {
                try
                {
                    ds = database.ExecuteDataSet(command);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return ds;
            }
        }

        public DataSet GetInventoryDetails(PagingParams reqModel)
        {
            string Connection = _iConfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("GetInventoryDetails");
            DataSet ds = new DataSet();
            using (command)
            {
                try
                {
                    database.AddInParameter(command, "@PageNumber", DbType.Int32, reqModel.PageNumber);
                    database.AddInParameter(command, "@PageSize", DbType.Int32, reqModel.PageSize);
                    ds = database.ExecuteDataSet(command);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return ds;
            }
        }

        public object InsertUpdateInventory()
        {
            string Connection = _iConfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("InventoryInUp");
            JObject Result = new JObject();
            using (command)
            {
                try
                {
                    database.AddParameter(command, "@InventoryID", System.Data.DbType.Int32, System.Data.ParameterDirection.InputOutput, "InventoryID", System.Data.DataRowVersion.Default, inventoryId);

                    database.AddInParameter(command, "@ProductId", DbType.Int32, productId);
                    database.AddInParameter(command, "@SupplierId", DbType.Int32, supplierId);
                    database.AddInParameter(command, "@Qty", DbType.Int32, quantity);
                    database.AddInParameter(command, "@Price", DbType.Decimal, price);
                    database.AddInParameter(command, "@CrModBy", DbType.String, createdModifyBy);
                    database.AddInParameter(command, "@OperationFlag", DbType.Int32, operationFlag);
                    database.AddOutParameter(command, "@Result", DbType.Int32, -1);
                    database.ExecuteNonQuery(command);

                    Result.Add("inventoryId", command.Parameters["@InventoryID"].Value.ToString());
                    Result.Add("result", command.Parameters["@Result"].Value.ToString());
                    return Result;
                }
                catch (Exception e)
                {
                    Result.Add("result", "-10");
                    Result.Add("message", e.Message);
                    return Result;
                }
            }
        }
    }
}
